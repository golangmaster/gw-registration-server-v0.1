package cmd

// Cmd flag params.
type Params struct {
	RpcPort  int
	AppsPort int
	UserName string
	Password string
	Token    string
}
