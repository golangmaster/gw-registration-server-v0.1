package grpc

import (
	"crypto/tls"
	"fmt"
	grpcAuth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/sirupsen/logrus"
	"gitlab.com/golangmaster/gw-registration-server/core/auth"
	"gitlab.com/golangmaster/gw-registration-server/core/auth/secure"
	"gitlab.com/golangmaster/gw-registration-server/core/cmd"
	"gitlab.com/golangmaster/gw-registration-server/core/model/sing"
	"gitlab.com/golangmaster/gw-registration-server/core/model/user"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"log"
	"net"

	"gitlab.com/golangmaster/gw-registration-server/core/proto"
)

// Params server.
type Params struct {
	Session *grpc.Server
	Listen  *net.Listener
}

// Google rpc running to server.
func Running(params *cmd.Params) *Params {

	// Full tcp address.
	addr := fmt.Sprintf("localhost:%d", params.RpcPort)

	// Listen new tcp.
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		logrus.Panic("Failed to listen:", err)
	}

	// Authenticator user
	auther := auth.Authenticator{
		Username: params.UserName,
		Password: params.Password,
		Token:    params.Token,
	}

	// TLS certificates configurations.
	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{secure.Cert},
		ClientCAs:    secure.CertPool,
		ClientAuth:   tls.VerifyClientCertIfGiven,
	}

	// gRPC new server session TLS.
	session := grpc.NewServer(
		grpc.Creds(credentials.NewTLS(tlsConfig)),
		grpc.StreamInterceptor(grpcAuth.StreamServerInterceptor(auther.Authenticate)),
		grpc.UnaryInterceptor(grpcAuth.UnaryServerInterceptor(auther.Authenticate)),
	)

	logrus.WithFields(logrus.Fields{
		"addr": addr,
	}).Info("Gateway rpc running to server")

	// Return params.
	return &Params{
		Session: session,
		Listen:  &lis,
	}

}

// The register service server.
func (p *Params) RegisterServiceServer() {

	err := p.Session.Serve(*p.Listen)
	if err != nil {
		log.Fatal(err)
	}

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Registration user service.
	api.RegisterUserServiceServer(p.Session, user.New())
	api.RegisterSingServer(p.Session, sing.NewSingApi(ctx))

}
