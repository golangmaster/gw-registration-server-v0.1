package secure

import (
	"crypto"
	"crypto/tls"
	"crypto/x509"
	"log"
	"os"
)

var (
	// Key is the private key
	Key crypto.PrivateKey
	// Cert is a self signed certificate
	Cert tls.Certificate
	// CertPool contains the self signed certificate
	CertPool *x509.CertPool
)

func init() {
	var err error

	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	Cert, err := tls.LoadX509KeyPair(dir+"/certs/master.cert", dir+"/certs/master.key")
	if err != nil {
		log.Fatalln("Failed to parse key pair:", err)
	}
	Cert.Leaf, err = x509.ParseCertificate(Cert.Certificate[0])
	if err != nil {
		log.Fatalln("Failed to parse certificate:", err)
	}

	CertPool = x509.NewCertPool()
	CertPool.AddCert(Cert.Leaf)

	Key = Cert.PrivateKey
}
