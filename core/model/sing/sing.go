package sing

import (
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	__sing "gitlab.com/golangmaster/gw-registration-server/core/proto"
)

var err error

type ParamsSing struct {
	ctx *context.Context
}

func NewSingApi(ctx context.Context) *ParamsSing {
	return &ParamsSing{
		ctx: &ctx,
	}
}

func (p *ParamsSing) In(ctx context.Context, req *__sing.SingRequest) (*__sing.SingResponse, error) {
	return nil, grpc.Errorf(codes.Unauthenticated, "authentication failed: %s", err)
}

func (p *ParamsSing) Up(ctx context.Context, req *__sing.SingRequest) (*__sing.SingResponse, error) {
	return nil, grpc.Errorf(codes.Unauthenticated, "authentication failed: %s", err)
}
