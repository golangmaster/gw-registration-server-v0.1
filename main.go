package main

import (
	"flag"
	"github.com/sirupsen/logrus"
	"gitlab.com/golangmaster/gw-registration-server/apps"
	"gitlab.com/golangmaster/gw-registration-server/core/cmd"
	"gitlab.com/golangmaster/gw-registration-server/core/grpc"
	"google.golang.org/grpc/grpclog"
	"io/ioutil"
	"os"
	"runtime"
)

func init() {

	// Running all processors.
	runtime.GOMAXPROCS(runtime.NumCPU())

	// Log set format.
	logrus.SetFormatter(&logrus.TextFormatter{
		DisableSorting: true,
	})

	// gRPC logger v2.
	grpclog.SetLoggerV2(grpclog.NewLoggerV2(os.Stdout, ioutil.Discard, ioutil.Discard))

}

func main() {

	logrus.Info("Server running...")

	// Command params.
	var params cmd.Params

	// Command flags.
	flag.IntVar(&params.RpcPort, "grpc-port", 50051, "The server grpc port running")
	flag.IntVar(&params.AppsPort, "apps-port", 9001, "The apps port running")
	flag.StringVar(&params.UserName, "username", "admin", "The username to authenticate with")
	flag.StringVar(&params.Password, "password", "admin", "The password to authenticate with")
	flag.StringVar(&params.Token, "token", "CPmwUFraJdISWQuDBEnI2lOPwQg8eN2gwmyD", "The token to authenticate with")
	flag.Parse()

	// Running grpc/api.
	go grpc.Running(&params).RegisterServiceServer()

	// Applications running.
	go apps.Running(&params)

	<-make(chan bool)

}
