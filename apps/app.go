package apps

import (
	"fmt"
	"github.com/gorilla/mux"

	"gitlab.com/golangmaster/gw-registration-server/core/cmd"
	"net/http"
)

func Running(params *cmd.Params) {

	// Create new router.
	r := mux.NewRouter()

	// Add of static folder.
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./apps/static/")))

	// Create new http server.
	_ = http.ListenAndServe(fmt.Sprintf(":%d", params.AppsPort), r)

}
